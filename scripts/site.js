/*
  @file JavaScript for Ron Swanson Quotes Assignment 3
  @author Rosemary Canon
  @copyright November 2020
 */

"use strict";
document.addEventListener("DOMContentLoaded", setup);

//This variable will hold the two elements we will be using: button and paragraph.
let global = {};

/**
 * Initialization of the page.
 */
function setup() {
  global.button = document.querySelector("button");
  global.quote = document.querySelector("#quote");
  //get the quotation marks
  global.leftqm = document.querySelector("#left-quote");
  global.rightqm = document.querySelector("#right-quote");
  global.button.addEventListener("click", getQuote);
}

/**
 * Gets quote from the Ron Swanson quote API.
 */
function getQuote() {
  let url = "https://ron-swanson-quotes.herokuapp.com/v2/quotes";
  fetch(url)
    .then(response => {
      if (!response.ok) {
        throw new Error("Status code: " + reponse.status);
      }
      //make the quation marks visible
      quotationMarkVisibility("visible");
      return response.json();
    })
    .then(json => displayQuote(json))
    .catch(error => treatError(error));
}

/**
 * Displays the quote in the DOM.
 * @param  {JSON} json JSON string containing an array holding the string quote
 */
function displayQuote(json) {
  global.quote.style.color = "black";
  global.quote.textContent = json[0];
}

/**
 * Treats the error returned from the Ron Swanson Quote API. The error will be shown in the console.
 * An error message in red will be shown to the user, instead of a quote. The quotation marks image
 * will also be removed to state that it isn't a quote, but rather an error.
 * @param  {Error} error Error returned if the GET request to the Ron Swanson Quote API was unsuccessul
 */
function treatError(error) {
  console.error(error);
  //set quotation marks to hidden
  quotationMarkVisibility("hidden");
  global.quote.style.color = "red";
  global.quote.textContent = "An error has occurred. Please try again.";
}

/**
 * Sets the visibility of the quotation mark images depending if the fetch
 * returned a response or an error. If a response.ok was returned then the
 * vibility will be set to "visible". If not, it will be set to hidden.
 * @param {String} state "hidden" or "visible"
 */
function quotationMarkVisibility(state) {
  global.leftqm.style.visibility = state;
  global.rightqm.style.visibility = state;
}
